import 'package:bloc/bloc.dart';
import 'package:chart_progress/helper/catch_exceptions.dart';
import 'package:chart_progress/logic/model/chart_model.dart';
import 'package:chart_progress/logic/repositories/repository.dart';
import 'package:dio/dio.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'get_chart_event.dart';
part 'get_chart_state.dart';
part 'get_chart_bloc.freezed.dart';

class GetChartBloc extends Bloc<GetChartEvent, GetChartState> {
  GetChartBloc() : super(const _Initial());
  UserRepository repository = UserRepository();

  @override
  Stream<GetChartState> mapEventToState(
    GetChartEvent event,
  ) async* {
    yield* event.map(getChart: _getChart);
  }

  Stream<GetChartState> _getChart(GetChartEvent event) async* {
    yield const GetChartState.initial();
    try {
      ChartModel model = await repository.getChart();
      yield GetChartState.loaded(model);
    } on DioError catch (e) {
      yield GetChartState.error(CatchException.convertException(e));
    }
  }
}
