part of 'get_chart_bloc.dart';

@freezed
class GetChartEvent with _$GetChartEvent {
  const factory GetChartEvent.getChart() = _GetChart;
}
