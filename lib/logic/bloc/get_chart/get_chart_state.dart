part of 'get_chart_bloc.dart';

@freezed
class GetChartState with _$GetChartState {
  const factory GetChartState.initial() = _Initial;
  const factory GetChartState.loaded(ChartModel model) = _Loaded;
  const factory GetChartState.error(CatchException mes) = _Error;
}
