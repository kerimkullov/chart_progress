import 'dart:convert';

ChartModel chartModelFromJson(String str) =>
    ChartModel.fromJson(json.decode(str));

String chartModelToJson(ChartModel data) => json.encode(data.toJson());

class ChartModel {
  ChartModel({
    this.price,
    this.changePercent,
    this.changeAmount,
    this.high,
    this.low,
    this.volumePrimary24H,
    this.volumeSecondary24H,
    this.points,
    this.pointsStartFrom,
  });

  double? price;
  double? changePercent;
  double? changeAmount;
  double? high;
  double? low;
  double? volumePrimary24H;
  double? volumeSecondary24H;
  List<double>? points;
  DateTime? pointsStartFrom;

  factory ChartModel.fromJson(Map<String, dynamic> json) => ChartModel(
        price: json["price"].toDouble(),
        changePercent: json["changePercent"].toDouble(),
        changeAmount: json["changeAmount"].toDouble(),
        high: json["high"].toDouble(),
        low: json["low"].toDouble(),
        volumePrimary24H: json["volumePrimary24h"].toDouble(),
        volumeSecondary24H: json["volumeSecondary24h"].toDouble(),
        points: List<double>.from(json["points"].map((x) => x.toDouble())),
        pointsStartFrom: DateTime.parse(json["pointsStartFrom"]),
      );

  Map<String, dynamic> toJson() => {
        "price": price,
        "changePercent": changePercent,
        "changeAmount": changeAmount,
        "high": high,
        "low": low,
        "volumePrimary24h": volumePrimary24H,
        "volumeSecondary24h": volumeSecondary24H,
        "points": List<dynamic>.from(points!.map((x) => x)),
        "pointsStartFrom": pointsStartFrom != null
            ? pointsStartFrom!.toIso8601String()
            : DateTime.now(),
      };
}
