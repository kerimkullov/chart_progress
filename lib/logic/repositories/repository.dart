import 'package:chart_progress/logic/model/chart_model.dart';
import 'package:chart_progress/logic/services/service.dart';

class UserRepository {
  final ServiceApi _serviceApi = ServiceApi();

  // get chart
  Future<ChartModel> getChart() => _serviceApi.getChart();
}
