import 'package:chart_progress/helper/catch_exceptions.dart';
import 'package:chart_progress/helper/dio_settings.dart';
import 'package:chart_progress/logic/model/chart_model.dart';
import 'package:dio/dio.dart';

class ServiceApi {
  late DioSettings _dioSettings;
  late Dio _dio;

  /// И мап для запроса
  late Map<String, dynamic> request;
  static final ServiceApi _instance = ServiceApi.internal();
  factory ServiceApi() => _instance;
  ServiceApi.internal() {
    _dioSettings = DioSettings();
    _dio = _dioSettings.dio;
  }
  Future<ChartModel> getChart() async {
    final response = await _dio
        .get("chart?model.range=1d&model.primary=Xbt&model.secondary=Aud");
    return ChartModel.fromJson(response.data);
  }
}
