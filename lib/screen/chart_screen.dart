import 'dart:math';

import 'package:chart_progress/constants/colors.dart';
import 'package:flutter/material.dart';

class ChartPainterScreen extends CustomPainter {
  final Color lineColor;
  final List<double> values;

  ChartPainterScreen(this.lineColor, this.values);

  @override
  void paint(Canvas canvas, Size size) {
    final paintLine = Paint()
      ..color = ChartColors.chartSide
      ..style = PaintingStyle.stroke
      ..strokeWidth = 3;
    final paint = Paint()
      ..color = lineColor
      ..style = PaintingStyle.fill;
    final path = Path();
    path.moveTo(0, size.height);

    final yMin = values.reduce(min);
    final yMax = values.reduce(max);
    final yHeight = yMax - yMin;
    final xAxisStep = size.width / values.length;
    var xValue = 0.0;
    for (var i = 0; i < values.length; i++) {
      final value = values[i];
      final yValue = yHeight == 0
          ? (0.5 * size.height)
          : ((yMax - value) / yHeight) * size.height;
      if (xValue == 0) {
        path.moveTo(xValue, yValue);
      } else {
        final previousValue = values[i - 1];
        final xPrevious = xValue - xAxisStep;
        final yPrevious = yHeight == 0
            ? (0.5 * size.height)
            : ((yMax - previousValue) / yHeight) * size.height;
        final controlPointX = xPrevious + (xValue - xPrevious) / 2;

        path.cubicTo(
            controlPointX, yPrevious, controlPointX, yValue, xValue, yValue);
      }
      xValue += xAxisStep;
    }

    path.lineTo(size.width, size.height);
    path.lineTo(0, size.height);
    path.close();

    canvas.drawPath(path, paint);
    canvas.drawPath(path, paintLine);
  }

  drawText(Canvas canvas, Offset position, double width, TextStyle style,
      String text) {
    final textSpan = TextSpan(text: text, style: style);
    final textPainter =
        TextPainter(text: textSpan, textDirection: TextDirection.ltr);
    textPainter.layout(minWidth: 0, maxWidth: width);
    textPainter.paint(canvas, position);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
