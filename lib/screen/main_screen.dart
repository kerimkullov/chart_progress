import 'package:chart_progress/components/again_screen.dart';
import 'package:chart_progress/components/custom_loading.dart';
import 'package:chart_progress/constants/colors.dart';
import 'package:chart_progress/logic/bloc/get_chart/get_chart_bloc.dart';
import 'package:chart_progress/logic/model/chart_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'chart_screen.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final bloc = GetChartBloc();
  List<double> list = [
    83396.89,
    82364.65,
    81545.52,
    82169.63,
    81390.36,
    81591.06,
    81923.55,
    81500.0,
    81050.0,
    81257.9,
    81385.01,
    82028.57,
    81845.01,
    82040.0,
    82267.56,
    82249.01,
    81891.57,
    81969.37,
    81780.0,
    81990.01,
    81529.27,
    80799.75,
    82615.06,
    82540.25
  ];

  @override
  void initState() {
    getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text(widget.title),
      ),
      body: Padding(
          padding: const EdgeInsets.all(8),
          child: BlocBuilder<GetChartBloc, GetChartState>(
            bloc: bloc,
            builder: (context, state) => state.maybeMap(
                orElse: () => AgainScreen(tryAgain: getData),
                loaded: (value) => _buildChartContainer(value.model),
                initial: (_) => const CustomLoading(),
                error: (e) => AgainScreen(tryAgain: getData)),
          )),
    );
  }

  void getData() {
    bloc.add(const GetChartEvent.getChart());
  }

  Widget _buildChartContainer(ChartModel state) {
    return Container(
      decoration:
          BoxDecoration(border: Border.all(color: Colors.green, width: 2)),
      padding: const EdgeInsets.all(8),
      height: 300,
      width: MediaQuery.of(context).size.width,
      child: CustomPaint(
        painter: ChartPainterScreen(ChartColors.chartFill, state.points ?? []),
      ),
    );
  }
}
