import 'package:flutter/material.dart';

class ChartColors {
  static const Color chartFill = Colors.orangeAccent;
  static const Color chartSide = Colors.orange;
}
